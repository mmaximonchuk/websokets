const WebSocket = require("ws");
const express = require("express");
const path = require("path");
const app = express();
const port = 9876;

app.use('/', express.static(path.resolve(__dirname, '../client')))

const server = app.listen(port, () => {
	console.log(`Server started on port: ${port}`);
});

const wss = new WebSocket.Server({
  noServer: true,
	// verifyClient: (info) => {
	// 	return true;
	// }
});

wss.on("connection", (ws) => {
  ws.on("message", (data) => {
    wss.clients.forEach((client) => {
      if (client.readyState === WebSocket.OPEN) {
        client.send(data);
      };
    });
  });
});


server.on('upgrade', async (request, socket, head) => {
	// return socket.end("HTTP/1.1 401 Unauthorized, 'ascii');
	

	wss.handleUpgrade(request, socket, head, (ws) => {
		wss.emit('connection', ws, request)
	})

})
