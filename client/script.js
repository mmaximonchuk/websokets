const server = new WebSocket("ws://localhost:9876/websoket");
const $ = (selector) => document.querySelector(selector);

const messages = $(".messages");
const sendBtn = $(".sendBtn");
const messageTextInput = $(".messageText");
const userMsgForm = $('.msgForm');

// client message handler
userMsgForm.onsubmit = (event) =>{ 
  event.preventDefault()
  const message = messageTextInput.value;
  if (message.length === 0) return;

  generateMessageEntry(message, "Client");
  server.send(message);
};

server.onopen = () => {
  sendBtn.disabled = false;
};

server.onmessage = async (event) => {
  const message = await event.data.text();
  console.log("message: ", message);
  generateMessageEntry(message, 'Server');

};

function generateMessageEntry(message, type) {
  const newTextNode = document.createElement("div");
  addClass(newTextNode, 'message');
  newTextNode.innerHTML = `${type}: ${message}`;
  clearMessageInput(messageTextInput);

  messages.insertAdjacentElement("beforeend", newTextNode);
}

function addClass(el, className) {
  el.classList.add(className);
}

function clearMessageInput(inputElement) {
  inputElement.value = "";
}
